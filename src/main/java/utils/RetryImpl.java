package utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryImpl implements IRetryAnalyzer{
	
	int retryCount = 1;

	public boolean retry(ITestResult result) {
		
		if(retryCount == 1) {
			retryCount++;
			return true;
		}		
		return false;
	}

}
