package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import io.restassured.RestAssured;
import io.restassured.authentication.BasicAuthScheme;
import io.restassured.authentication.PreemptiveBasicAuthScheme;

public class PreAndPost extends Reporter{
	
	public static String url, username, password, excelName;
	
	public PreAndPost() {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("./src/main/resources/config.properties")));
			url = prop.getProperty("URL");
		//	username = prop.getProperty("UserName");
		//	password = prop.getProperty("Password");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeClass
	public void initializeReport() {
		startTestModule(testCaseName, testDescription);	
		
	}
	
	@BeforeMethod
	public void preRequest() {		
		test = startTestCase(testNodes);
		test.assignCategory(category);
		test.assignAuthor(authors);
		RestAssured.baseURI= url;
	/*	BasicAuthScheme auth = new BasicAuthScheme();
		auth.setUserName(username);
		auth.setPassword(password);
		System.out.println(username);*/
	
		/*
		
		PreemptiveBasicAuthScheme base = new PreemptiveBasicAuthScheme();
		base.setUserName(username);
		base.setPassword(password);
		RestAssured.authentication = base;
*/
	
	
	
	}
	
	
	@DataProvider(name="fetchData",parallel=false)
	public  Object[][] getData(){
		return utils.DataProvider.readExcel(excelName);		
	}	
	
	@BeforeSuite
	public void beforeSuite(){
		startResult();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}

	@AfterSuite
	public void afterSuite(){
		endResult();
	}

	@AfterTest
	public void afterTest(){
	}

	@AfterMethod
	public void afterMethod(){
		endTestcase();
	}

}
