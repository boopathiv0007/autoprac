package servicenow.requests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PostNewIncident extends ResponseValidations{

	@BeforeTest
	public void setValues() {
		testCaseName = "Post New Incident";
		testNodes = "Post";
		testDescription = "Post New Incident in ServiceNow";
		category = "Smoke";
		authors = "Babu";
		excelName = "TC002";
	}

	@Test(dataProvider="fetchData")
	public void getIncidents(String shortDesc, String category, String urgency) {
			
		
		String body = "{\"short_description\":\""+shortDesc+"\","+
					   "\"category\":\""+category+"\","+
					   "\"urgency\":\""+urgency+"\"}";
		
		Response response = 	
				RestAssured
				.given()
				.headers("Content-Type","application/json")
				.body(body)
				.post("/api/now/table/incident")
				.then()
				.extract().response();
				
		verifyResponseStatusCode(response,201);
		verifyResponseStatus(response, "Created");
		String firstResponseData = getResponseData(response, "result.number");
		System.out.println(firstResponseData);
		
	}
}