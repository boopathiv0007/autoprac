package servicenow.requests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.authentication.BasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ServiceNowDelete 	extends ResponseValidations{

		@BeforeTest
		public void setValues() {
			testCaseName = "Post New Incident";
			testNodes = "Post";
			testDescription = "Post New Incident in ServiceNow";
			category = "Smoke";
			authors = "Babu";
			excelName = "TC002";
		}
  @Test
  public void f() {
	//  RestAssured.baseURI= "https://dev56077.service-now.com/";
		
	

		Response response = 	RestAssured
				.given()
				.headers("Content-Type","application/json")
				
				.body("{\"short_description\":\"stars\",\"description\":\"Standard\"}")

				
				.queryParam("sysparm_fields", "number,sys_id")
				.post("api/now/table/incident")
				.then()
				.extract().response();
				
		System.out.println(response.body().prettyPrint());
		if(response.statusCode() == 201) {
			System.out.println("Success with requests");
		}else {
			System.out.println("Response failed with status code "+response.statusCode() + "and the response error is "+response.prettyPrint());
			throw new RuntimeException();
		}
		
		JsonPath jsonResponse = response.body().jsonPath();
		
		String sysnum = jsonResponse.get("result.sys_id");
		System.out.println(sysnum);
		
		
		
		
	
		
	Response responses = 	RestAssured
				.given()
			.headers("Content-Type","application/json")
			//	.body("{\"short_description\":\"stars\",\"description\":\"Standard\"}")	
				//.queryParam("sysparm_fields", "sys_id")
				.delete("api/now/table/incident/"+sysnum+"")
				.then()
				.extract().response();
	

		
	}
}
		