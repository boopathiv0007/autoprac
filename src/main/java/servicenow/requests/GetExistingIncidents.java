package servicenow.requests;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetExistingIncidents extends ResponseValidations{
//
	@BeforeTest
	public void setValues() {
		testCaseName = "Get All Incidents";
		testNodes = "Get";
		testDescription = "Get All Incidents from ServiceNow";
		category = "Smoke";
		authors = "Babu";
		excelName = "TC001";
	}

	@Test(dataProvider="fetchData")
	public void getIncidents(String severity, String category, String urgency) {
			
		// Send request and store response
		Response response = 	
				RestAssured
				.given()
				.param("severity", severity)
				.param("category", category)
				.param("urgency", urgency)
				.get("/api/now/table/incident")
				.then()
				.extract().response();
	
		// Verify the status code
		verifyResponseStatusCode(response,200);
		
		// Verify the status
		verifyResponseStatus(response, "OK");
		
		// Get the incident number
		String firstResponseData = getFirstResponseData(response, "result.number");
		System.out.println(firstResponseData);
		
		// Verify the response with the param
		Map<String, String> expected = new HashMap<String, String>();
		expected.put("result.severity", severity);
		expected.put("result.category", category);
		expected.put("result.urgency", urgency);
		verifyResponseListData(response, expected);
		
		
	}
}