package slack;

import java.io.File;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.restassured.module.jsv.JsonSchemaValidator;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class postchannelist2  extends ResponseValidations{
	
	
	@BeforeTest
	public void setValues() {
		testCaseName = "post channel";
		testNodes = "Get";
		testDescription = "post channel";
		category = "Smoke";
		authors = "Babu";
		excelName = "TC001";
	}
	@Test
	public void set(){
	//	RestAssured.baseURI= "https://testleaf-webservices.slack.com";
//	System.out.println(SessionId);
System.out.println("*****");
//Cookie sessioncookies=	new Cookie
//.Builder("token", "xoxp-376021344903-389083173589-388573525697-6082d41525ad8471b6b0c21270fd651e")
//.setSecured(true)
//.build();

String token="xoxp-376021344903-389083173589-388573525697-6082d41525ad8471b6b0c21270fd651e";



JsonSchemaValidator.matchesJsonSchema(new File("./data/createchannel.json"));


Response response= RestAssured
.given()
.headers("Content-Type","application/json")
.and()


//.params("token","xoxp-376021344903-389083173589-388573525697-6082d41525ad8471b6b0c21270fd651e")

.header("Authorization", "Bearer "+token)
.body(new File("./data/createchannel.json"))



//.params("Authorization", "Bearer "+token)

//.params("limit","50")

//.cookie(sessioncookies)
.post("/api/conversations.create")

//.get("/api/conversations.list?limit=50&token=xoxp-376021344903-389083173589-388573525697-6082d41525ad8471b6b0c21270fd651e")
.then()
.extract().response();
System.out.println(response.jsonPath().prettyPrint());
System.out.println(response.getStatusLine());
System.out.println(response.getCookies());
//System.out.println(SessionId);
	

verifyResponseStatusCode(response,200);

	
	}
}
