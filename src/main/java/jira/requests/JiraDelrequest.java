package jira.requests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class JiraDelrequest extends ResponseValidations{

	@BeforeTest
	public void setValues() {
		testCaseName 	= "Get All Issues";
		testNodes 		= "Get";
		testDescription = "Get All Incidents from ServiceNow";
		category 		= "Smoke";
		authors 		= "Babu";
		excelName 		= "TC004";
	}
	@Test
  public void passdel() {
		
		Response response = 	
				RestAssured
				.given()
				.param("priority", "High")
				.headers("Content-Type","application/json")
				.delete("/rest/api/2/issue/RES-4")
				.then()
				.extract().response();
		verifyResponseStatusCode(response,204);
		
	}
}
		
  

