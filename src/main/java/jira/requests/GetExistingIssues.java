package jira.requests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetExistingIssues extends ResponseValidations{

	@BeforeTest
	public void setValues() {
		testCaseName 	= "Get All Issues";
		testNodes 		= "Get";
		testDescription = "Get All Incidents from ServiceNow";
		category 		= "Smoke";
		authors 		= "Babu";
		excelName 		= "TC004";
	}

	@Test(dataProvider="fetchData")
	public void getIncidents(String issue) {
			
		// Send request and store response
		Response response = 	
				RestAssured
				.given()
				.param("priority", "High")
				.get("rest/api/2/search?jql=project=WEB")
				.then()
				.extract().response();
		
		verifyResponseStatus(response, "OK");
		
		
	}
}