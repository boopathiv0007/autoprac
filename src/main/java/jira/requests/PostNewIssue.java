package jira.requests;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import base.ResponseValidations;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PostNewIssue extends ResponseValidations{

	@BeforeTest
	public void setValues() {
		testCaseName = "Post New Issue";
		testNodes = "Post";
		testDescription = "Post New Issue in Jira";
		category = "Smoke";
		authors = "Babu";
		excelName = "TC003";
	}

	@Test(dataProvider="fetchData")
	public void getIncidents(String shortDesc, String summary, String type) throws IOException {
			
	
		Response response = 	
				RestAssured
				.given()
				.headers("Content-Type","application/json")
				.body(new File("./data/issue.json"))
				.post("rest/api/2/issue/")
				.then()
				.extract().response();
				
		System.out.println(response.prettyPrint());
		System.out.println(response.jsonPath().get("id"));
		
				
	}
}