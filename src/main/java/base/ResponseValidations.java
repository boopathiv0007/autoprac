package base;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utils.PreAndPost;

public class ResponseValidations extends PreAndPost{
	
	public boolean verifyResponseStatusCode(Response response, int expectedStatusCode) {
		if(response.statusCode() == expectedStatusCode) {
			reportStep("The expected response code :"+expectedStatusCode +" matches the actual", "PASS");
			return true;
		}else {
			reportStep("The expected response code :"+expectedStatusCode +" does not matches the actual "+response.statusCode(), "FAIL");
			return false;
		}
	}

	public boolean verifyResponseStatus(Response response, String statusInfo) {
		if(response.statusLine().contains(statusInfo)) {
			reportStep("The expected response :"+statusInfo +" matches the actual", "PASS");
			return true;
		}else {
			reportStep("The expected response  :"+statusInfo +" does not matches the actual "+response.statusLine(), "FAIL");
			return false;
		}
	}
	
	public boolean verifyResponseTime(Response response, int expectedResponseTime) {
		if(response.getTime() < expectedResponseTime) {
			reportStep("The actual response time :"+response.getTime() +" is within the expected "+expectedResponseTime, "PASS");
			return true;
		}else {
			reportStep("The actual response time  :"+response.getTime() +" is more than the expected "+expectedResponseTime, "FAIL");
			return false;
		}
	}
	
	public String getResponseData(Response response, String what) {
		JsonPath jsonResponse = response.body().jsonPath();
		String data = jsonResponse.get(what);
		if(data.equals("null")) {
			reportStep("The expected data "+what+" is not found  in the response.", "INFO");
		}
		return data;
	}
	
	public List<String> getResponseListData(Response response, String what) {
		JsonPath jsonResponse = response.body().jsonPath();
		List<String> data = jsonResponse.getList(what);
		if(data.size() == 0) {
			reportStep("The expected data "+what+" is not found  in the response.", "INFO");
		}
		return data;
	}
	
	public String getFirstResponseData(Response response, String what) {
		JsonPath jsonResponse = response.body().jsonPath();
		List<String> data = jsonResponse.getList(what);
		if(data.size() == 0) {
			reportStep("The expected data "+what+" is not found in the response.", "FAIL");
		}
		return data.get(0);
	}
	
	public boolean verifyResponseData(Response response, Map<String, String> expected) {
		JsonPath jsonResponse = response.body().jsonPath();
		boolean bMatch = true;
		for (Entry<String, String> entry : expected.entrySet()) {
			if(!jsonResponse.get(entry.getKey()).equals(entry.getValue())) {
				reportStep("The response value for key: "+entry.getKey()+" :-"+entry.getValue()+" did not match with expected value "+jsonResponse.get(entry.getKey()), "FAIL");
				bMatch = false;
			}
		}
		
		if(bMatch) {
			reportStep("The response had all values matched with expected","PASS");
			return true;
		}
		
		return false;
	}
	
	public boolean verifyResponseListData(Response response, Map<String, String> expected) {
		JsonPath jsonResponse = response.body().jsonPath();
		
		boolean bMatch = true;
		for (Entry<String, String> entry : expected.entrySet()) {
			List<String> responseList = jsonResponse.getList(entry.getKey());
			System.out.println(responseList.size());
			for (String responseValue : responseList) {
				if(!responseValue.equals(entry.getValue())) {
					reportStep("The response value for key: "+entry.getKey()+" :-"+entry.getValue()+" did not match with expected value "+responseValue, "FAIL");
					bMatch = false;
				}
			}				
		}
		
		if(bMatch) {
			reportStep("The response had all values matched with expected","PASS");
			return true;
		}
		
		return false;
	}
}
